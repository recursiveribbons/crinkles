# Contributing

First, thanks for your interest in contributing to the project!

To contribute, fork the project, develop on the `dev` branch, then create a pull request towards the `dev` branch on
this project. I will then merge it to the `main` branch when it's ready to deploy.

The `dev` branch generates a Docker image with the `:dev` tag. This can be used to test the functionality.

Please remember to never EVER put API keys in your repositories. That's why the configuration for them are all
externalized.

To keep your fork up to date, consider adding this as a remote
with `git remote add upstream git@gitlab.com:recursiveribbons/crinkles.git` and pull periodically, especially before
pushing.
