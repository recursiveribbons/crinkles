package dev.robinsyl.crinkles.services;

import dev.robinsyl.crinkles.db.ChangeRepository;
import dev.robinsyl.crinkles.db.DiaperRepository;
import dev.robinsyl.crinkles.types.Change;
import dev.robinsyl.crinkles.types.Diaper;
import dev.robinsyl.crinkles.types.User;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class ChangeService {
    private static final Logger logger = Logger.getLogger(ChangeService.class.getName());

    private final DiaperRepository diaperRepository;
    private final ChangeRepository changeRepository;

    @Autowired
    public ChangeService(DiaperRepository diaperRepository, ChangeRepository changeRepository, MeterRegistry meterRegistry) {
        this.diaperRepository = diaperRepository;
        this.changeRepository = changeRepository;
        meterRegistry.gauge("bot.changes", changeRepository, ChangeRepository::count);
    }

    /**
     * Adds a diaper change for an owner with a list of diapers and a given time
     *
     * @param owner       User to add change for
     * @param diaperNames List of short names of diapers included in the change, repeat if multiple used.
     * @param changeTime  When the change happened
     * @return The change added, empty if failed.
     * @throws IllegalArgumentException A diaper with the specified name and owner is not found
     */
    public Optional<Change> addChange(User owner, List<String> diaperNames, LocalDateTime changeTime) {
        Map<Diaper, Integer> diapers = new HashMap<>();
        diaperNames.stream()
                .map(name -> {
                    Optional<Diaper> diaper = diaperRepository.findByOwnerAndShortName(owner, name);
                    if (diaper.isEmpty()) {
                        throw new IllegalArgumentException("A diaper with the name " + name + " was not found.");
                    }
                    return diaper.get();
                })
                .forEach(d -> diapers.compute(d, (k, v) -> v == null ? 1 : v + 1));
        return addChange(owner, diapers, changeTime);
    }

    /**
     * Adds a diaper change for an owner with a list of diapers and a given time
     *
     * @param owner      User to add change for
     * @param diapers    Map of diapers used in the change, and amount used.
     * @param changeTime When the change happened
     * @return The change added, empty if failed.
     */
    public Optional<Change> addChange(User owner, Map<Diaper, Integer> diapers, LocalDateTime changeTime) {
        Change change = new Change(changeTime, owner, diapers);
        changeRepository.save(change);
        diapers.forEach((d, i) -> {
            d.useStock(i);
            diaperRepository.save(d);
        });
        return changeRepository.findById(change.getId());
    }

    /**
     * Get the latest change of a user.
     *
     * @param owner The user to look up
     * @return An Optional of the latest change, empty if not found.
     */
    public Optional<Change> getLatestChange(User owner) {
        return changeRepository.findTopByOwnerOrderByChangeTimeDesc(owner);
    }

    /**
     * Remove the latest change of a user.
     *
     * @param owner The user to remove change from
     */
    public void removeLatestChange(User owner) {
        getLatestChange(owner).ifPresent(changeRepository::delete);
    }

    /**
     * Find users that are overdue for a change.
     *
     * @param changeReminderTime Time in hours for how long until they need to be reminded
     * @param updateInterval     Time in minutes how often this check is done
     * @return A stream of users that need a change
     */
    public List<User> findOverdueChanges(long changeReminderTime, long updateInterval) {
        LocalDateTime end = LocalDateTime.now()
                .truncatedTo(ChronoUnit.MINUTES)
                .minus(changeReminderTime, ChronoUnit.HOURS);
        LocalDateTime start = end.minusMinutes(updateInterval);
        logger.log(Level.INFO, "Performing overdue check for [{0}, {1}]", new Object[]{start, end});
        return changeRepository.findByChangeTimeBetween(start, end)
                .stream()
                .map(Change::getOwner)
                .toList();
    }
}
