package dev.robinsyl.crinkles.services;

import dev.robinsyl.crinkles.db.DiaperRepository;
import dev.robinsyl.crinkles.types.Diaper;
import dev.robinsyl.crinkles.types.User;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DiaperService {

    private final DiaperRepository diaperRepository;

    @Autowired
    public DiaperService(DiaperRepository diaperRepository, MeterRegistry meterRegistry) {
        this.diaperRepository = diaperRepository;
        meterRegistry.gauge("bot.diapers", diaperRepository, DiaperRepository::count);
    }

    /**
     * List a user's diaper collection
     *
     * @param owner The user whose collection to fetch
     * @return A list of the user's diapers.
     */
    public List<Diaper> getDiapers(User owner) {
        return diaperRepository.findByOwner(owner);
    }

    /**
     * Get a diaper from a user's collection
     *
     * @param owner     The user whose collection to search
     * @param shortName The name of the diaper
     * @return An Optional containing the diaper. Empty if not found.
     */
    public Optional<Diaper> getDiaper(User owner, String shortName) {
        return diaperRepository.findByOwnerAndShortName(owner, shortName);
    }

    /**
     * Adds a diaper to a user's collection
     *
     * @param owner     The user whose collection to add the diaper to
     * @param shortName The name of the diaper
     * @throws IllegalArgumentException A diaper with the specified name and owner already exists
     */
    public void addDiaper(User owner, String shortName) {
        if (diaperRepository.findByOwnerAndShortName(owner, shortName).isPresent()) {
            throw new IllegalArgumentException("A diaper with this name already exists.");
        }
        diaperRepository.save(new Diaper(shortName, shortName, owner));
    }

    /**
     * Add to a diaper's remaining stock
     *
     * @param owner           The user whose collection to add the diaper to
     * @param shortName       The name of the diaper
     * @param additionalStock The amount of stock to add
     * @return The remaining stock after restocking
     */
    public int restockDiaper(User owner, String shortName, int additionalStock) {
        Diaper diaper = diaperRepository.findByOwnerAndShortName(owner, shortName)
                .orElseThrow(() -> new IllegalArgumentException("A diaper with this name was not found."));
        diaper.restock(additionalStock);
        return diaperRepository.save(diaper).getStock();
    }
}
