package dev.robinsyl.crinkles.services;

import dev.robinsyl.crinkles.db.UserRepository;
import dev.robinsyl.crinkles.types.User;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository, MeterRegistry meterRegistry) {
        this.userRepository = userRepository;
        meterRegistry.gauge("bot.users", userRepository, UserRepository::count);
    }

    public User getUser(org.telegram.telegrambots.meta.api.objects.User telegramUser) {
        return userRepository.findById(telegramUser.getId())
                .orElseGet(() -> userRepository.save(User.of(telegramUser)));
    }
}
