package dev.robinsyl.crinkles.db;

import dev.robinsyl.crinkles.types.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {
}
