package dev.robinsyl.crinkles.db;

import dev.robinsyl.crinkles.types.Change;
import dev.robinsyl.crinkles.types.User;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ChangeRepository extends CrudRepository<Change, UUID> {
    Optional<Change> findTopByOwnerOrderByChangeTimeDesc(User owner);

    List<Change> findByChangeTimeBetween(LocalDateTime start, LocalDateTime end);
}
