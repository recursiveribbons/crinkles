package dev.robinsyl.crinkles.db;

import dev.robinsyl.crinkles.types.Diaper;
import dev.robinsyl.crinkles.types.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface DiaperRepository extends CrudRepository<Diaper, UUID> {
    List<Diaper> findByOwner(User owner);

    Optional<Diaper> findByOwnerAndShortName(User owner, String shortName);
}
