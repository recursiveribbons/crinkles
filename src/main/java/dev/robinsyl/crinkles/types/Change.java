package dev.robinsyl.crinkles.types;

import jakarta.annotation.Nonnull;
import jakarta.persistence.*;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

@Entity
public class Change {
    @Id
    @Column(columnDefinition = "uuid", updatable = false, nullable = false)
    private UUID id;
    @Column(nullable = false)
    private LocalDateTime changeTime;
    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User owner;
    @ElementCollection(fetch = FetchType.EAGER)
    @Nonnull
    private Map<Diaper, Integer> diapersUsed;

    public Change(LocalDateTime changeTime, User owner, @Nonnull Map<Diaper, Integer> diapersUsed) {
        this.id = UUID.randomUUID();
        this.changeTime = changeTime;
        this.owner = owner;
        this.diapersUsed = diapersUsed;
    }

    @SuppressWarnings("java:S2637")
    protected Change() {
        // required by JPA
    }

    public UUID getId() {
        return id;
    }

    public LocalDateTime getChangeTime() {
        return changeTime;
    }

    public void setChangeTime(LocalDateTime changeTime) {
        this.changeTime = changeTime;
    }

    public User getOwner() {
        return owner;
    }

    public Map<Diaper, Integer> getDiapersUsed() {
        return Collections.unmodifiableMap(diapersUsed);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Change change = (Change) o;
        return id.equals(change.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Change{" +
                "id=" + id +
                ", changeTime=" + changeTime +
                ", owner=" + owner +
                ", diapersUsed=" + diapersUsed +
                '}';
    }
}
