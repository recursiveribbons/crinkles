package dev.robinsyl.crinkles.types;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;

import java.util.Objects;

@Entity(name = "users")
public class User {
    @Id
    @Column(nullable = false)
    private Long id;
    @Column(nullable = false)
    private String firstName;
    private String lastName;
    private String username;
    private String languageCode;

    public User(Long id, String firstName, String lastName, String username, String languageCode) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.languageCode = languageCode;
    }

    protected User() {
        // required by JPA
    }

    public static User of(org.telegram.telegrambots.meta.api.objects.User telegramUser) {
        return new User(telegramUser.getId(), telegramUser.getFirstName(), telegramUser.getLastName(), telegramUser.getUserName(), telegramUser.getLanguageCode());
    }

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public org.telegram.telegrambots.meta.api.objects.User toTelegramUser() {
        return new org.telegram.telegrambots.meta.api.objects.User(id, firstName, false, lastName, username, languageCode, null, null, null, null, null, null);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id.equals(user.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", username='" + username + '\'' +
                ", languageCode='" + languageCode + '\'' +
                '}';
    }
}
