package dev.robinsyl.crinkles.types;

import jakarta.persistence.*;

import java.util.Objects;
import java.util.UUID;

@Entity
public class Diaper {
    @Id
    @Column(columnDefinition = "uuid", updatable = false, nullable = false)
    private UUID id;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private String shortName;
    @Column(nullable = false)
    private Integer stock;
    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User owner;

    public Diaper(String name, String shortName, User owner) {
        this(name, shortName, owner, 0);
    }

    public Diaper(String name, String shortName, User owner, int stock) {
        this.id = UUID.randomUUID();
        this.name = name;
        this.shortName = shortName;
        this.stock = stock;
        this.owner = owner;
    }

    protected Diaper() {
        // required by JPA
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public Integer getStock() {
        return stock;
    }

    public void restock(int additionalStock) {
        this.stock += additionalStock;
    }

    public void useStock(int usedStock) {
        this.stock -= usedStock;
    }

    public User getOwner() {
        return owner;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Diaper diaper = (Diaper) o;
        return id.equals(diaper.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Diaper{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", shortName='" + shortName + '\'' +
                ", stock=" + stock +
                ", owner=" + owner +
                '}';
    }
}
