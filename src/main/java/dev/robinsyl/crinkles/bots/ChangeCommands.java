package dev.robinsyl.crinkles.bots;

import dev.robinsyl.crinkles.services.ChangeService;
import dev.robinsyl.crinkles.services.UserService;
import dev.robinsyl.crinkles.types.Change;
import dev.robinsyl.crinkles.types.Diaper;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.abilitybots.api.objects.Ability;
import org.telegram.telegrambots.abilitybots.api.objects.MessageContext;
import org.telegram.telegrambots.abilitybots.api.util.AbilityExtension;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static org.telegram.telegrambots.abilitybots.api.objects.Locality.USER;
import static org.telegram.telegrambots.abilitybots.api.objects.Privacy.PUBLIC;

@Component
public class ChangeCommands extends BaseCommands implements AbilityExtension {

    private final UserService userService;
    private final ChangeService changeService;
    private final Counter statusCounter;
    private final Counter changeCounter;
    private final Counter undoCounter;

    @Autowired
    public ChangeCommands(MeterRegistry meterRegistry, MessageSource messageSource, UserService userService, ChangeService changeService) {
        super(meterRegistry, messageSource);
        this.userService = userService;
        this.changeService = changeService;
        this.statusCounter = createCounter("status");
        this.changeCounter = createCounter("change");
        this.undoCounter = createCounter("undo");
    }

    @SuppressWarnings("unused")
    public Ability getStatus() {
        return Ability.builder()
                .name("status")
                .info("Get the current diaper status")
                .input(0)
                .locality(USER)
                .privacy(PUBLIC)
                .action(ctx -> changeService.getLatestChange(userService.getUser(ctx.user())).ifPresentOrElse(
                        change -> formatChange(change, ctx),
                        () -> sendString("bot.status.no_change", ctx)
                ))
                .post(ctx -> statusCounter.increment())
                .build();
    }

    @SuppressWarnings("unused")
    public Ability addChange() {
        return Ability.builder()
                .name("change")
                .info("Add a change")
                .input(0)
                .locality(USER)
                .privacy(PUBLIC)
                .action(ctx -> {
                    List<String> diaperNames = Arrays.asList(ctx.arguments());
                    try {
                        Optional<Change> change = changeService.addChange(userService.getUser(ctx.user()), diaperNames, LocalDateTime.now());
                        change.ifPresentOrElse(c -> {
                                    int amount = c.getDiapersUsed().size();
                                    if (amount > 0) {
                                        sendString("bot.change.added", ctx, amount);
                                        notifyIfEmpty(c.getDiapersUsed().keySet(), ctx);
                                    } else {
                                        sendString("bot.change.not_wearing", ctx);
                                    }
                                },
                                () -> sendString("bot.error", ctx));
                    } catch (IllegalArgumentException e) {
                        sendString("bot.diaper.not_found", ctx);
                    }
                })
                .post(ctx -> changeCounter.increment())
                .build();
    }

    @SuppressWarnings("unused")
    public Ability undoChange() {
        return Ability.builder()
                .name("undo")
                .info("Undo previous change")
                .input(0)
                .locality(USER)
                .privacy(PUBLIC)
                .action(ctx -> {
                    changeService.removeLatestChange(userService.getUser(ctx.user()));
                    sendString("bot.change.removed", ctx);
                })
                .post(ctx -> undoCounter.increment())
                .build();
    }

    private void formatChange(Change change, MessageContext ctx) {
        LocalDateTime changeTime = change.getChangeTime();
        LocalDateTime now = LocalDateTime.now();
        long hours = changeTime.until(now, ChronoUnit.HOURS);
        changeTime = changeTime.plusHours(hours);
        long minutes = changeTime.until(now, ChronoUnit.MINUTES);
        String diapers;
        if (change.getDiapersUsed().isEmpty()) {
            sendString("bot.status.not_wearing", ctx, hours, minutes);
            return;
        } else {
            diapers = change.getDiapersUsed()
                    .entrySet().stream()
                    .map(d -> String.format("%s (%d)", d.getKey().getName(), d.getValue()))
                    .collect(Collectors.joining(", "));
        }
        sendString("bot.status.change", ctx, diapers, hours, minutes);
    }

    void notifyIfEmpty(Set<Diaper> diapers, MessageContext ctx) {
        diapers.stream()
                .filter(d -> d.getStock() <= 5)
                .forEach(d -> sendString("bot.diaper.low_stock", ctx, d.getName(), d.getStock()));
    }
}
