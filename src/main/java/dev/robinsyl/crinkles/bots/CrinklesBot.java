package dev.robinsyl.crinkles.bots;

import org.mapdb.DBMaker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.abilitybots.api.bot.AbilityBot;
import org.telegram.telegrambots.abilitybots.api.db.MapDBContext;
import org.telegram.telegrambots.abilitybots.api.objects.Ability;
import org.telegram.telegrambots.abilitybots.api.sender.SilentSender;
import org.telegram.telegrambots.abilitybots.api.util.AbilityExtension;
import org.telegram.telegrambots.client.okhttp.OkHttpTelegramClient;
import org.telegram.telegrambots.longpolling.interfaces.LongPollingUpdateConsumer;
import org.telegram.telegrambots.longpolling.starter.SpringLongPollingBot;

import java.util.List;
import java.util.logging.Logger;

import static java.util.logging.Level.INFO;
import static org.telegram.telegrambots.abilitybots.api.objects.Locality.USER;
import static org.telegram.telegrambots.abilitybots.api.objects.Privacy.PUBLIC;

@Component
public class CrinklesBot extends AbilityBot implements SpringLongPollingBot {
    private static final Logger log = Logger.getLogger(CrinklesBot.class.getName());

    private final String botToken;
    private final long creatorId;

    @Autowired
    public CrinklesBot(@Value("${bot.token}") String botToken, @Value("${bot.username}") String botUsername, @Value("${bot.creator}") Long creatorId, List<AbilityExtension> extensions) {
        super(new OkHttpTelegramClient("botToken"), botUsername);
        this.botToken = botToken;
        this.creatorId = creatorId;
        if (!extensions.isEmpty()) {
            addExtensions(extensions);
            log.log(INFO, "Loaded extensions: {0}", extensions);
        }
    }

    private CrinklesBot(SilentSender silent, List<AbilityExtension> extensions) {
        super(null, "TestBot", new MapDBContext(DBMaker.memoryDB().closeOnJvmShutdown().make()));
        this.botToken = null;
        this.creatorId = 24601L;
        this.telegramClient = null;
        this.silent = silent;
        addExtensions(extensions);
        super.onRegister();
    }

    protected static CrinklesBot getTestInstance(SilentSender silent, List<AbilityExtension> extensions) {
        return new CrinklesBot(silent, extensions);
    }

    @SuppressWarnings("unused")
    public Ability ping() {
        return Ability.builder()
                .name("ping")
                .info("Check if the bot is online")
                .input(0)
                .locality(USER)
                .privacy(PUBLIC)
                .action(ctx -> silent.send("Pong", ctx.chatId()))
                .build();
    }

    @Override
    public long creatorId() {
        return creatorId;
    }

    @Override
    public void onRegister() {
        super.onRegister();
        silent.send("Bot has been restarted.", creatorId);
    }

    @Override
    public String getBotToken() {
        return botToken;
    }

    @Override
    public LongPollingUpdateConsumer getUpdatesConsumer() {
        return this;
    }
}
