package dev.robinsyl.crinkles.bots;

import dev.robinsyl.crinkles.services.ChangeService;
import dev.robinsyl.crinkles.types.User;
import io.micrometer.core.instrument.MeterRegistry;
import jakarta.annotation.PreDestroy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.abilitybots.api.objects.MessageContext;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.util.List;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Service
@Profile("!dev")
public class NotificationService extends BaseCommands {
    private static final long SEND_DELAY = 50L; // milliseconds
    private final long changeReminderTime; // hours
    private final long updateInterval; // minutes

    private final CrinklesBot bot;
    private final ChangeService service;

    private final ScheduledExecutorService executor;
    private final Random random = new Random();

    @Autowired
    public NotificationService(MeterRegistry meterRegistry, MessageSource messageSource, CrinklesBot bot, ChangeService service, @Value("${bot.notification.change-duration}") long reminderTime, @Value("${bot.notification.interval}") long interval) {
        super(meterRegistry, messageSource);
        this.bot = bot;
        this.service = service;
        changeReminderTime = reminderTime;
        updateInterval = interval;
        executor = Executors.newSingleThreadScheduledExecutor();
    }

    @Scheduled(cron = "${bot.notification.cron}")
    public void notifyChanges() {
        List<User> users = service.findOverdueChanges(changeReminderTime, updateInterval);
        for (int i = 0; i < users.size(); i++) {
            User user = users.get(i);
            int stringNumber = random.nextInt(1, 5); // 1 - 4
            MessageContext ctx = MessageContext.newContext(new Update(), user.toTelegramUser(), user.getId(), bot);
            executor.schedule(() -> sendString("bot.change.remind." + stringNumber, ctx), i * SEND_DELAY, TimeUnit.MILLISECONDS);
        }
    }

    @PreDestroy
    public void onDestroy() {
        executor.shutdown();
    }
}
