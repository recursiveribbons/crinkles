package dev.robinsyl.crinkles.bots;

import dev.robinsyl.crinkles.services.DiaperService;
import dev.robinsyl.crinkles.services.UserService;
import dev.robinsyl.crinkles.types.User;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.abilitybots.api.objects.Ability;
import org.telegram.telegrambots.abilitybots.api.util.AbilityExtension;

import java.util.stream.Collectors;

import static org.telegram.telegrambots.abilitybots.api.objects.Locality.USER;
import static org.telegram.telegrambots.abilitybots.api.objects.Privacy.PUBLIC;

@Component
public class DiaperCommands extends BaseCommands implements AbilityExtension {

    private final UserService userService;
    private final DiaperService diaperService;
    private final Counter diapersCounter;
    private final Counter diaperCounter;
    private final Counter addDiaperCounter;
    private final Counter restockCounter;

    @Autowired
    public DiaperCommands(MeterRegistry meterRegistry, MessageSource messageSource, UserService userService, DiaperService diaperService) {
        super(meterRegistry, messageSource);
        this.userService = userService;
        this.diaperService = diaperService;
        this.diapersCounter = createCounter("diapers");
        this.diaperCounter = createCounter("diaper");
        this.addDiaperCounter = createCounter("adddiaper");
        this.restockCounter = createCounter("restock");
    }

    @SuppressWarnings("unused")
    public Ability getDiapers() {
        return Ability.builder()
                .name("diapers")
                .info("Get info about your diaper stash")
                .input(0)
                .locality(USER)
                .privacy(PUBLIC)
                .action(ctx -> {
                    String diapers = diaperService.getDiapers(userService.getUser(ctx.user())).stream()
                            .map(diaper -> getString("bot.diaper.status", ctx, diaper.getName(), diaper.getShortName(), diaper.getStock()))
                            .collect(Collectors.joining("\n"));
                    if (diapers.isEmpty()) {
                        sendString("bot.diaper.empty", ctx);
                        return;
                    }
                    sendFormatted(diapers, ctx);
                })
                .post(ctx -> diapersCounter.increment())
                .build();
    }

    @SuppressWarnings("unused")
    public Ability getDiaper() {
        return Ability.builder()
                .name("diaper")
                .info("Get info about a diaper")
                .input(1)
                .locality(USER)
                .privacy(PUBLIC)
                .action(ctx -> {
                    String diaperName = ctx.firstArg();
                    diaperService.getDiaper(userService.getUser(ctx.user()), diaperName).ifPresentOrElse(
                            diaper -> sendString("bot.diaper.status", ctx, diaper.getName(), diaper.getShortName(), diaper.getStock()),
                            () -> sendString("bot.diaper.not_found", ctx)
                    );
                })
                .post(ctx -> diaperCounter.increment())
                .build();
    }

    @SuppressWarnings("unused")
    public Ability addDiaper() {
        return Ability.builder()
                .name("adddiaper")
                .info("Add a diaper to your collection")
                .input(0)
                .locality(USER)
                .privacy(PUBLIC)
                .action(ctx -> {
                    User user = userService.getUser(ctx.user());
                    if (ctx.arguments().length != 1) {
                        sendString("bot.diaper.invalid_name", ctx);
                        return;
                    }
                    String diaperName = ctx.firstArg();
                    try {
                        diaperService.addDiaper(user, diaperName);
                        sendString("bot.diaper.added", ctx, diaperName);
                    } catch (IllegalArgumentException e) {
                        sendString("bot.diaper.already_exists", ctx);
                    }
                })
                .post(ctx -> addDiaperCounter.increment())
                .build();
    }

    @SuppressWarnings("unused")
    public Ability restockDiaper() {
        return Ability.builder()
                .name("restock")
                .info("Restock a diaper")
                .input(2)
                .locality(USER)
                .privacy(PUBLIC)
                .action(ctx -> {
                    String diaperName = ctx.firstArg();
                    int additionalStock = Integer.parseInt(ctx.secondArg());
                    try {
                        int stock = diaperService.restockDiaper(userService.getUser(ctx.user()), diaperName, additionalStock);
                        sendString("bot.diaper.restocked", ctx, diaperName, stock);
                    } catch (IllegalArgumentException e) {
                        sendString("bot.diaper.not_found", ctx);
                    }
                })
                .post(ctx -> restockCounter.increment())
                .build();
    }
}
