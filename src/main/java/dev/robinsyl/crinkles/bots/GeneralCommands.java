package dev.robinsyl.crinkles.bots;

import dev.robinsyl.crinkles.services.UserService;
import dev.robinsyl.crinkles.types.User;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.abilitybots.api.objects.Ability;
import org.telegram.telegrambots.abilitybots.api.util.AbilityExtension;

import static org.telegram.telegrambots.abilitybots.api.objects.Locality.USER;
import static org.telegram.telegrambots.abilitybots.api.objects.Privacy.PUBLIC;

@Component
public class GeneralCommands extends BaseCommands implements AbilityExtension {

    private final UserService userService;
    private final Counter startCounter;
    private final Counter helpCounter;
    private final Counter aboutCounter;

    @Autowired
    public GeneralCommands(MeterRegistry meterRegistry, MessageSource messageSource, UserService userService) {
        super(meterRegistry, messageSource);
        this.userService = userService;
        this.startCounter = createCounter("start");
        this.helpCounter = createCounter("help");
        this.aboutCounter = createCounter("about");
    }

    @SuppressWarnings("unused")
    public Ability start() {
        return Ability.builder()
                .name("start")
                .info("Start the bot")
                .input(0)
                .locality(USER)
                .privacy(PUBLIC)
                .action(ctx -> {
                    User user = userService.getUser(ctx.user());
                    sendString("bot.start", ctx, user.getFirstName());
                })
                .post(ctx -> startCounter.increment())
                .build();
    }

    @SuppressWarnings("unused")
    public Ability help() {
        return Ability.builder()
                .name("help")
                .info("How to use")
                .input(0)
                .locality(USER)
                .privacy(PUBLIC)
                .action(ctx -> sendString("bot.help", ctx))
                .post(ctx -> helpCounter.increment())
                .build();
    }

    @SuppressWarnings("unused")
    public Ability about() {
        return Ability.builder()
                .name("about")
                .info("About this bot")
                .input(0)
                .locality(USER)
                .privacy(PUBLIC)
                .action(ctx -> sendString("bot.about", ctx))
                .post(ctx -> aboutCounter.increment())
                .build();
    }
}
