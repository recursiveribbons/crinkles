package dev.robinsyl.crinkles.bots;

import dev.robinsyl.crinkles.services.DiaperService;
import dev.robinsyl.crinkles.services.UserService;
import dev.robinsyl.crinkles.types.Diaper;
import io.micrometer.core.instrument.MeterRegistry;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.arrayWithSize;
import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class DiaperCommandsTest extends CommandsTest {

    @Mock
    MeterRegistry meterRegistry;
    @Mock
    UserService userService;
    @Mock
    DiaperService diaperService;

    DiaperCommands commands;

    @BeforeEach
    void setUp() {
        super.setUp();
        commands = new DiaperCommands(meterRegistry, messageSource, userService, diaperService);
        when(userService.getUser(telegramUser)).thenReturn(user);
    }

    @Test
    void getDiapers() {
        Diaper diaper = new Diaper("Crinkles", "crinkles", user);
        Diaper booster = new Diaper("Booster", "booster", user);
        when(diaperService.getDiapers(user)).thenReturn(List.of(diaper, booster));

        commands.getDiapers().action().accept(getContext());
        String result = getMessage();

        assertThat(result.split("\n"), arrayWithSize(2));
        assertThat(result, containsString("Crinkles"));
        assertThat(result, containsString("Booster"));
    }

    @Test
    void getDiapersEmpty() {
        when(diaperService.getDiapers(user)).thenReturn(Collections.emptyList());

        commands.getDiapers().action().accept(getContext());
        String result = getMessage();

        assertThat(result, matchesLocalized("bot.diaper.empty"));
    }

    @Test
    void getDiaper() {
        Diaper diaper = new Diaper("Crinkles", "crinkles", user, 42);
        when(diaperService.getDiaper(user, "crinkles")).thenReturn(Optional.of(diaper));

        commands.getDiaper().action().accept(getContext("crinkles"));
        String result = getMessage();

        assertThat(result, matchesLocalized("bot.diaper.status", "Crinkles", "crinkles", 42));
    }

    @Test
    void getDiaperNotFound() {
        when(diaperService.getDiaper(user, "crinkles")).thenReturn(Optional.empty());

        commands.getDiaper().action().accept(getContext("crinkles"));
        String result = getMessage();

        assertThat(result, matchesLocalized("bot.diaper.not_found"));
    }

    @Test
    void addDiaper() {
        commands.addDiaper().action().accept(getContext("crinkles"));
        String result = getMessage();

        verify(diaperService).addDiaper(user, "crinkles");
        assertThat(result, matchesLocalized("bot.diaper.added", "crinkles"));
    }

    @Test
    void addDiaperInvalidName() {
        commands.addDiaper().action().accept(getContext("Super", "Diaper"));
        String result = getMessage();

        assertThat(result, matchesLocalized("bot.diaper.invalid_name"));
    }

    @Test
    void addDiaperAlreadyExists() {
        doThrow(IllegalArgumentException.class).when(diaperService).addDiaper(user, "crinkles");
        commands.addDiaper().action().accept(getContext("crinkles"));
        String result = getMessage();

        assertThat(result, matchesLocalized("bot.diaper.already_exists"));
    }

    @Test
    void restockDiaper() {
        when(diaperService.restockDiaper(user, "crinkles", 42)).thenReturn(42);
        commands.restockDiaper().action().accept(getContext("crinkles", "42"));
        String result = getMessage();

        verify(diaperService).restockDiaper(user, "crinkles", 42);
        assertThat(result, matchesLocalized("bot.diaper.restocked", "crinkles", 42));
    }

    @Test
    void restockDiaperNotFound() {
        when(diaperService.restockDiaper(user, "crinkles", 42)).thenThrow(IllegalArgumentException.class);
        commands.restockDiaper().action().accept(getContext("crinkles", "42"));
        String result = getMessage();

        assertThat(result, matchesLocalized("bot.diaper.not_found"));
    }
}
