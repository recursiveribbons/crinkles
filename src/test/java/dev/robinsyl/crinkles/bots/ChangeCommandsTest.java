package dev.robinsyl.crinkles.bots;

import dev.robinsyl.crinkles.services.ChangeService;
import dev.robinsyl.crinkles.services.UserService;
import dev.robinsyl.crinkles.types.Change;
import dev.robinsyl.crinkles.types.Diaper;
import io.micrometer.core.instrument.MeterRegistry;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ChangeCommandsTest extends CommandsTest {

    @Mock
    MeterRegistry meterRegistry;
    @Mock
    UserService userService;
    @Mock
    ChangeService changeService;

    ChangeCommands commands;

    @BeforeEach
    void setUp() {
        super.setUp();
        commands = new ChangeCommands(meterRegistry, messageSource, userService, changeService);
        when(userService.getUser(telegramUser)).thenReturn(user);
    }

    @Test
    void getStatus() {
        Diaper diaper = new Diaper("Crinkles", "crinkles", user);
        Diaper booster = new Diaper("Booster", "booster", user);
        Change change = new Change(LocalDateTime.now(), user, Map.of(diaper, 1, booster, 2));
        when(changeService.getLatestChange(user)).thenReturn(Optional.of(change));

        commands.getStatus().action().accept(getContext());
        String message = getMessage();

        assertThat(message, containsString("Crinkles (1)"));
        assertThat(message, containsString("Booster (2)"));
    }

    @Test
    void getStatusNoChange() {
        when(changeService.getLatestChange(user)).thenReturn(Optional.empty());

        commands.getStatus().action().accept(getContext());
        String message = getMessage();

        assertThat(message, matchesLocalized("bot.status.no_change"));
    }

    @Test
    void getStatusWearingNothing() {
        Change change = new Change(LocalDateTime.now(), user, Map.of());
        when(changeService.getLatestChange(user)).thenReturn(Optional.of(change));

        commands.getStatus().action().accept(getContext());
        String message = getMessage();

        assertThat(message, containsString("nothing"));
    }

    @Test
    void addChange() {
        Diaper diaper = new Diaper("Crinkles", "crinkles", user, 42);
        Diaper booster = new Diaper("Booster", "booster", user, 42);
        Change change = new Change(LocalDateTime.now(), user, Map.of(diaper, 1, booster, 2));
        when(changeService.addChange(eq(user), eq(List.of("crinkles", "booster")), any())).thenReturn(Optional.of(change));

        commands.addChange().action().accept(getContext("crinkles", "booster"));
        String message = getMessage();

        verify(changeService).addChange(eq(user), eq(List.of("crinkles", "booster")), any());
        assertThat(message, matchesLocalized("bot.change.added", 2));
    }

    @Test
    void addChangeLowStock() {
        Diaper diaper = new Diaper("Crinkles", "crinkles", user, 7);
        Diaper booster = new Diaper("Booster", "booster", user, 3);
        Change change = new Change(LocalDateTime.now(), user, Map.of(diaper, 1, booster, 2));
        when(changeService.addChange(eq(user), eq(List.of("crinkles", "booster")), any())).thenReturn(Optional.of(change));

        commands.addChange().action().accept(getContext("crinkles", "booster"));
        verify(silent, times(2)).execute(messageCaptor.capture());
        List<SendMessage> messages = messageCaptor.getAllValues();
        messages.forEach(m -> {
            assertThat(m.getParseMode(), equalTo(ParseMode.HTML));
            assertThat(m.getText(), notNullValue());
        });
        assertThat(messages.get(0).getText(), matchesLocalized("bot.change.added", 2));
        assertThat(messages.get(1).getText(), matchesLocalized("bot.diaper.low_stock", "Booster", 3));
    }

    @Test
    void addChangeEmpty() {
        Change change = new Change(LocalDateTime.now(), user, Map.of());
        when(changeService.addChange(eq(user), eq(List.of()), any())).thenReturn(Optional.of(change));

        commands.addChange().action().accept(getContext());
        String message = getMessage();

        assertThat(message, matchesLocalized("bot.change.not_wearing", 2));
    }

    @Test
    void addChangeNotFound() {
        when(changeService.addChange(eq(user), eq(List.of("brinkles", "booster")), any())).thenThrow(IllegalArgumentException.class);

        commands.addChange().action().accept(getContext("brinkles", "booster"));
        String message = getMessage();

        assertThat(message, matchesLocalized("bot.diaper.not_found", 2));
    }

    @Test
    void addChangeUnknownError() {
        when(changeService.addChange(eq(user), eq(List.of("crinkles", "booster")), any())).thenReturn(Optional.empty());

        commands.addChange().action().accept(getContext("crinkles", "booster"));
        String message = getMessage();

        assertThat(message, matchesLocalized("bot.error", 2));
    }

    @Test
    void undoChange() {
        commands.undoChange().action().accept(getContext());
        String message = getMessage();

        verify(changeService).removeLatestChange(user);
        assertThat(message, matchesLocalized("bot.change.removed"));
    }
}
