package dev.robinsyl.crinkles.bots;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;
import org.springframework.context.MessageSource;

import java.util.Locale;

public class MatchesLocalized extends TypeSafeMatcher<String> {

    private final MessageSource source;
    private final String key;
    private final Object[] args;

    public MatchesLocalized(MessageSource source, String key, Object[] args) {
        this.source = source;
        this.key = key;
        this.args = args;
    }

    @Override
    protected boolean matchesSafely(String s) {
        return getMessage().equals(s);
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("a string containing ")
                .appendValue(getMessage());
    }

    private String getMessage() {
        return source.getMessage(key, args, Locale.ENGLISH);
    }
}
