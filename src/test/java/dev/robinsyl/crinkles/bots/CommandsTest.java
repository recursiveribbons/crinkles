package dev.robinsyl.crinkles.bots;

import dev.robinsyl.crinkles.types.User;
import org.hamcrest.Matcher;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.springframework.context.MessageSource;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.telegram.telegrambots.abilitybots.api.objects.MessageContext;
import org.telegram.telegrambots.abilitybots.api.sender.SilentSender;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.nio.charset.StandardCharsets;
import java.util.Collections;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.Mockito.verify;

@SuppressWarnings("java:S2187")
public class CommandsTest {
    final org.telegram.telegrambots.meta.api.objects.User telegramUser = new org.telegram.telegrambots.meta.api.objects.User(24601L, "Jean", false, "Valjean", "jean", "en", true, true, true, true, true, true);
    final User user = new User(24601L, "Jean", "Valjean", "jean", "en");

    @Mock
    SilentSender silent;
    @Captor
    ArgumentCaptor<SendMessage> messageCaptor;
    CrinklesBot bot;
    MessageSource messageSource;

    void setUp() {
        bot = CrinklesBot.getTestInstance(silent, Collections.emptyList());
        messageSource = createMessageSource();
    }

    MessageContext getContext(String... args) {
        return MessageContext.newContext(new Update(), telegramUser, 24601L, bot, args);
    }

    String getMessage() {
        verify(silent).execute(messageCaptor.capture());
        SendMessage sendMessage = messageCaptor.getValue();
        assertThat(sendMessage.getParseMode(), equalTo(ParseMode.HTML));
        assertThat(sendMessage.getText(), notNullValue());

        return sendMessage.getText();
    }

    private MessageSource createMessageSource() {
        ResourceBundleMessageSource source = new ResourceBundleMessageSource();
        source.setBasenames("localization/Strings");
        source.setDefaultEncoding(StandardCharsets.UTF_8.name());
        source.setUseCodeAsDefaultMessage(true);
        return source;
    }

    Matcher<String> matchesLocalized(String key, Object... args) {
        return new MatchesLocalized(messageSource, key, args);
    }
}
