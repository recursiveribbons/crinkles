package dev.robinsyl.crinkles.bots;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.telegram.telegrambots.abilitybots.api.objects.MessageContext;
import org.telegram.telegrambots.abilitybots.api.sender.SilentSender;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;

import java.util.Collections;

import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class CrinklesBotTest {

    @Mock
    SilentSender silent;

    CrinklesBot bot;

    @BeforeEach
    void setUp() {
        bot = CrinklesBot.getTestInstance(silent, Collections.emptyList());
    }

    @Test
    void botOnline() {
        MessageContext context = MessageContext.newContext(new Update(), new User(246L, "John", false), 24601L, bot);
        bot.ping().action().accept(context);
        verify(silent).send("Pong", 24601L);
    }

}
