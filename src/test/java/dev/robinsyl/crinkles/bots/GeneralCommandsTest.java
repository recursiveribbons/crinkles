package dev.robinsyl.crinkles.bots;

import dev.robinsyl.crinkles.services.UserService;
import io.micrometer.core.instrument.MeterRegistry;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.telegram.telegrambots.abilitybots.api.objects.MessageContext;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class GeneralCommandsTest extends CommandsTest {

    @Mock
    MeterRegistry meterRegistry;
    @Mock
    UserService userService;
    GeneralCommands commands;
    MessageContext context;

    @BeforeEach
    void setUp() {
        super.setUp();
        commands = new GeneralCommands(meterRegistry, messageSource, userService);
        context = getContext();
    }

    @Test
    void start() {
        when(userService.getUser(telegramUser)).thenReturn(user);
        commands.start().action().accept(context);
        String result = getMessage();

        assertThat(result, matchesLocalized("bot.start", user.getFirstName()));
    }

    @Test
    void help() {
        commands.help().action().accept(context);
        String result = getMessage();

        assertThat(result, matchesLocalized("bot.help"));
    }

    @Test
    void about() {
        commands.about().action().accept(context);
        String result = getMessage();

        assertThat(result, matchesLocalized("bot.about"));
    }
}
