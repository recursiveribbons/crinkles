package dev.robinsyl.crinkles;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

import java.util.Optional;

public class IsPresentMatcher extends TypeSafeMatcher<Optional<?>> {

    public static IsPresentMatcher isPresent() {
        return new IsPresentMatcher();
    }

    @Override
    protected boolean matchesSafely(Optional<?> s) {
        return s.isPresent();
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("a present Optional");
    }
}
