package dev.robinsyl.crinkles.services;

import dev.robinsyl.crinkles.db.ChangeRepository;
import dev.robinsyl.crinkles.db.DiaperRepository;
import dev.robinsyl.crinkles.types.Change;
import dev.robinsyl.crinkles.types.Diaper;
import dev.robinsyl.crinkles.types.User;
import io.micrometer.core.instrument.MeterRegistry;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import static dev.robinsyl.crinkles.IsPresentMatcher.isPresent;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@DataJpaTest
class ChangeServiceTest {
    static long CHANGE_REMINDER_TIME = 8L;
    static long UPDATE_INTERVAL = 10L;
    Logger logger = Logger.getLogger(ChangeServiceTest.class.getName());

    TestEntityManager entityManager;
    DiaperRepository diaperRepository;

    ChangeService service;

    @Autowired
    public ChangeServiceTest(TestEntityManager entityManager, DiaperRepository diaperRepository, ChangeRepository changeRepository) {
        this.entityManager = entityManager;
        this.diaperRepository = diaperRepository;
        service = new ChangeService(diaperRepository, changeRepository, Mockito.mock(MeterRegistry.class));
    }

    @BeforeEach
    void setUp() {
        entityManager.clear();
    }

    @AfterEach
    void tearDown() {
        entityManager.clear();
    }

    @Test
    void findOverdueChanges() {
        User user1 = new User(1L, "John", "Doe", "johndoe", "en");
        User user2 = new User(2L, "Rae", "Doe", "raethedoe", "en");
        Diaper diaper1 = new Diaper("Crinkles", "crinkles", user1);
        Diaper diaper2 = new Diaper("Crinkles", "crinkles", user2);
        LocalDateTime now = LocalDateTime.now();
        Change change = new Change(now.minusHours(1L), user1, Map.of(diaper1, 1));
        Change overdueChange = new Change(now.minusHours(8L).minusMinutes(1L), user1, Map.of(diaper2, 1));
        logger.log(Level.INFO, "Change: {0}, Overdue change: {1}", new Object[]{now.minusHours(1L), now.minusHours(8L).minusMinutes(1L)});
        entityManager.persist(user1);
        entityManager.persist(user2);
        entityManager.persist(diaper1);
        entityManager.persist(diaper2);
        entityManager.persist(change);
        entityManager.persist(overdueChange);
        entityManager.flush();

        List<User> overdueChanges = service.findOverdueChanges(CHANGE_REMINDER_TIME, UPDATE_INTERVAL);

        assertThat(overdueChanges, hasSize(1));
        User user = overdueChanges.get(0);
        assertThat(user.getId(), equalTo(1L));
    }

    @Test
    void addChangeWithMultipleDiapers() {
        User user = new User(1L, "John", "Doe", "johndoe", "en");
        Diaper diaper = new Diaper("Crinkles", "crinkles", user, 42);
        Diaper booster = new Diaper("Booster", "booster", user, 50);
        entityManager.persist(user);
        diaperRepository.save(diaper);
        diaperRepository.save(booster);
        entityManager.flush();

        service.addChange(user, List.of("crinkles", "booster", "crinkles", "booster", "booster"), LocalDateTime.now());
        Optional<Change> optional = service.getLatestChange(user);

        assertThat(optional, isPresent());
        Change result = optional.orElseThrow();
        Map<Diaper, Integer> diapersUsed = result.getDiapersUsed();
        assertThat(diapersUsed, aMapWithSize(2));
        assertThat(diapersUsed, hasEntry(diaper, 2));
        assertThat(diapersUsed, hasEntry(booster, 3));
        assertThat(entityManager.find(Diaper.class, diaper.getId()).getStock(), equalTo(40));
        diapersUsed.forEach((k, i) -> {
            if (k.getName().equals("Crinkles")) {
                assertThat(k.getStock(), equalTo(40));
            } else {
                assertThat(k.getStock(), equalTo(47));
            }
        });
    }
}
