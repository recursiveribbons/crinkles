# Crinkles

A Telegram bot for tracking diaper changes. It is currently located under https://t.me/CrinklesBot.

## Usage

_This guide is for how to run an instance of the bot. For help on using the bot, run the `/help` command in the bot, or
ask in the [support chat](https://t.me/RobinDevChat)._

### From the registry

1. Create a new bot with the [BotFather](https://t.me/BotFather)
2. Make a copy of [docker-compose.yml](docker-compose.yml) on your server and fill in the environment variables
3. Run `docker-compose up -d`

### Build the container yourself

Same steps as _From the registry_, but with the file [docker-compose.build.yml](docker-compose.build.yml).

### Environment variables

- `BOT_TOKEN`: The bot token retrieved from the BotFather
- `BOT_USERNAME`: The username you gave the bot, without the @ symbol.
- `BOT_CREATOR`: Your user ID. You can retrieve this via the various user ID bots on Telegram
- `DB_PASSWORD`: The database password, must match `POSTGRES_PASSWORD`.
