FROM gradle:8.8-jdk21-alpine AS buildstep
WORKDIR /app
COPY . .
RUN gradle bootJar

FROM eclipse-temurin:21-jre-alpine
COPY --from=buildstep /app/build/libs/*.jar /opt/crinkles/lib/app.jar
ENTRYPOINT ["java"]
CMD ["-jar", "/opt/crinkles/lib/app.jar"]
